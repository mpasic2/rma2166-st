package ba.etf.rma21.projekat

import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter
import android.graphics.BitmapRegionDecoder.newInstance
import android.media.ImageReader.newInstance
import android.media.ImageWriter.newInstance
import android.net.sip.SipManager.newInstance
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceInfo.newInstance
import android.net.wifi.p2p.nsd.WifiP2pServiceRequest.newInstance
import android.net.wifi.p2p.nsd.WifiP2pUpnpServiceInfo.newInstance
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.view.isEmpty
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.FragmentKvizovi.Companion.kviz1
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.repositories.*
import ba.etf.rma21.projekat.view.*
import ba.etf.rma21.projekat.viewModel.QuizListViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.reflect.Array.newInstance
import java.net.URLClassLoader.newInstance
import javax.xml.datatype.DatatypeFactory.newInstance
import javax.xml.parsers.DocumentBuilderFactory.newInstance
import javax.xml.transform.TransformerFactory.newInstance
import javax.xml.validation.SchemaFactory.newInstance
import javax.xml.xpath.XPathFactory.newInstance


class MainActivity : AppCompatActivity() {

    private lateinit var bottomNav: BottomNavigationView
    private lateinit var kontejner: FrameLayout
    private lateinit var kontejnerPokusaja : FrameLayout


    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.kvizovi -> {
                val fragmetKvizovi = FragmentKvizovi.newInstance()
                openFragment(fragmetKvizovi)
                return@OnNavigationItemSelectedListener true
            }
            R.id.predmeti -> {
                val fragmetPredmeti = FragmentPredmeti.newInstance()
                openFragment(fragmetPredmeti)
                return@OnNavigationItemSelectedListener true
            }
            R.id.zaustaviKviz -> {
                bottomNav.selectedItemId = R.id.kvizovi
            }

            R.id.predajKviz -> {
                val porukica = Bundle()
                porukica.putString("ovoJePoruka","Završili ste kviz ${kviz1.naziv} sa tačnosti ")
                val frgmPoruka = FragmentPoruka.newInstance()
                frgmPoruka.arguments=porukica
                //openFragment(FragmentPoruka.newInstance())

                supportFragmentManager?.beginTransaction()?.replace(R.id.container, frgmPoruka, "fragment_screen")?.commit( )
            }




        }
        false
    }









    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()


    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {


        super.onCreate(savedInstanceState)


            setContentView(R.layout.activity_main)

            kontejner = findViewById(R.id.container)
            bottomNav = findViewById(R.id.bottomNav)
            bottomNav.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
            bottomNav.selectedItemId = R.id.kvizovi

            kontejner.addOnLayoutChangeListener(View.OnLayoutChangeListener { v, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
                var dajFragment = FragmentKvizovi.newInstance()


            })
        DBRepository.setContext(this.applicationContext)
        KvizRepository.setContext(this.applicationContext)
        PredmetRepository.setContext(this.applicationContext)
        OdgovorRepository.setContext(this.applicationContext)
        PitanjeKvizRepository.setContext(this.applicationContext)
        PredmetIGrupaRepository.setContext(this.applicationContext)
        TakeKvizRepository.setContext(this.applicationContext)
        AccountRepository.setContext(this.applicationContext)
        ApiConfig.setContext(this.applicationContext)
        OdgovorRepository.setContext(this.applicationContext)

        val intent: Intent = this.intent

            if (intent.data != null) intent.getStringExtra("payload")
                ?.let {  GlobalScope.launch(Dispatchers.Main) { AccountRepository.postaviHash(it)}    }





        val favoritesFragment = FragmentKvizovi.newInstance()
            openFragment(favoritesFragment)


    }


    override fun onBackPressed() {
        val currentFragment = supportFragmentManager.fragments.last()

        if(currentFragment is FragmentPitanje || currentFragment is FragmentPoruka || currentFragment is FragmentPokusaj){
            bottomNav.selectedItemId = R.id.kvizovi
        }
         if(bottomNav.selectedItemId == R.id.predmeti){
            bottomNav.selectedItemId = R.id.kvizovi

        }
        else if(bottomNav.selectedItemId == R.id.kvizovi){
            bottomNav.selectedItemId = R.id.predmeti

        }




    }








}

