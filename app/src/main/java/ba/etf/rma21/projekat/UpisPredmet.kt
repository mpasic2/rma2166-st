/*
package ba.etf.rma21.projekat

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.data.dajSveKvizoveZaPredmetizaGrupu
import ba.etf.rma21.projekat.data.dajSvePredmete
import ba.etf.rma21.projekat.data.dajSvePredmetePoGodini
import ba.etf.rma21.projekat.data.listaSvihUpisanihPredmeta
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.GrupaRepository
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import ba.etf.rma21.projekat.data.repositories.PredmetRepository

class UpisPredmet : Fragment(), AdapterView.OnItemSelectedListener {

    private lateinit var spinerGodina: Spinner
    private lateinit var  spinerPredmet: Spinner
    private lateinit var spinerGrupa: Spinner
    private lateinit var dodajDugme: Button


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) : View? {
        var view:View = inflater.inflate(R.layout.add_class_activity, container, false)

        spinerGodina = view.findViewById(R.id.odabirGodina)
        spinerPredmet = view.findViewById(R.id.odabirPredmet)
        spinerGrupa = view.findViewById(R.id.odabirGrupa)
        dodajDugme = view.findViewById(R.id.dodajPredmetDugme)


        ArrayAdapter.createFromResource(view.context, R.array.dropDownGodine,android.R.layout.simple_spinner_dropdown_item).also {
            adap -> adap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinerGodina.adapter = adap
        }

        spinerGodina.onItemSelectedListener=this
        spinerPredmet.onItemSelectedListener=this

        dodajDugme.setOnClickListener {
            //System.out.println("OVO RADIIIIIIIIIIIIIIIIIIII!")
            //Toast.makeText(this@UpisiActivity, "HALOOOO", Toast.LENGTH_SHORT).show()
            //spasavamo odabrane podatke
            val godina = spinerGodina.selectedItem.toString()
            val predmet = spinerPredmet.selectedItem.toString()
            val grupa = spinerGrupa.selectedItem.toString()


            KvizRepository.listaSvihMojihKvizova.addAll(dajSveKvizoveZaPredmetizaGrupu(predmet,grupa))
            val pred = Predmet(predmet,godina.toInt())
            listaSvihUpisanihPredmeta.add(pred)
            //finish()
            //onRestart()




        }

        return view

    }


    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        //za sve
        if(parent?.id?.equals(R.id.odabirGodina)!!) {
            val listaPredmetaPoGodinama = dajSvePredmetePoGodini(position + 1)
            val praznaLista: MutableList<String> = mutableListOf()

            val listaPredmeta : MutableList<String> = mutableListOf()
            for(predmet in listaSvihUpisanihPredmeta){
                listaPredmeta.add(predmet.naziv)
            }

            for (predmet in listaPredmetaPoGodinama) {
                praznaLista.add(predmet.naziv)
            }
            val listaPredmetaKojeTrebaPrikazat: MutableList<String> = mutableListOf()
            for(predm in praznaLista){
                if(listaPredmeta.contains(predm)) continue
                else listaPredmetaKojeTrebaPrikazat.add(predm)
            }


            //spinerPredmet.adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, listaPredmetaKojeTrebaPrikazat)
        }

        if(parent?.id?.equals(R.id.odabirPredmet)!!) {
            val listaGrupaPoPredmetima = GrupaRepository.getGroupsByPredmet(parent.selectedItem.toString()).toMutableList()
            val praznaLista: MutableList<String> = mutableListOf()
            for (grupe in listaGrupaPoPredmetima) {
                praznaLista.add(grupe.naziv)
            }

            //spinerGrupa.adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, praznaLista)
        }

    }


}
*/
