package ba.etf.rma21.projekat.data


import android.os.Build
import androidx.annotation.RequiresApi
import ba.etf.rma21.projekat.data.models.*
import ba.etf.rma21.projekat.data.repositories.AccountRepository
import retrofit2.Response
import retrofit2.http.*
import java.time.LocalDate
import java.time.LocalDateTime

interface Api {


    @RequiresApi(Build.VERSION_CODES.O)
    @GET("/account/{id}/lastUpdate")
    suspend fun accZadnjiUpdate(
            @Path("id") id: String = AccountRepository.getHash(), @Query("date") date : String ) : Response<ChangedObject>


    //PREDMET
    @GET("/predmet")
    suspend fun getSviPredmeti(): Response<List<Predmet>>

    @GET("/predmet/{id}")
    suspend fun getPredmetZaId(@Path("id") id: Int): Response<Predmet>


    //GRUPA
    @GET("/kviz/{id}/grupa")
    suspend fun getGrupeUkojimaJeKvizDostupan(@Path("id") id: Int): Response<List<Grupa>>

    @POST("/grupa/{gid}/student/{id}")
    suspend fun addStudentWithHash(@Path("gid") gid: Int, @Path("id") id: String = AccountRepository.getHash()): Response<Message>

    @GET("/student/{id}/grupa")
    suspend fun getGroupWithStudent(@Path("id") id: String = AccountRepository.getHash()): Response<List<Grupa>>

    @GET("/grupa")
    suspend fun getSveGrupe(): Response<List<Grupa>>

    @GET("/grupa/{id}")
    suspend fun getGrupaZaId(@Path("id") id: Int): Response<Grupa>

    @GET("/predmet/{id}/grupa")
    suspend fun getAllGrupeNaPredmetuZaId(@Path("id") id: Int): Response<List<Grupa>>



    //KVIZ
    @GET("/kviz")
    suspend fun getSveKvizove(): Response<List<Kviz>>

    @GET("/kviz/{id}")
    suspend fun getKvizZaId(@Path("id") id: Int): Response<Kviz>

    @GET("/grupa/{id}/kvizovi")
    suspend fun getKvizoviZaGrupu(@Path("id") id: Int): Response<List<Kviz>>


    //ODGOVOR
    @GET("/student/{id}/kviztaken/{ktid}/odgovori")
    suspend fun getOdgovoriZaKviz(@Path("ktid") ktid: Int, @Path("id") id: String = AccountRepository.getHash()): Response<List<Odgovor>>

    @POST("/student/{id}/kviztaken/{ktid}/odgovor")
    suspend fun addAnswerForTry(@Path("ktid") ktid: Int, @Body postaviOdg:PostaviOdg, @Path("id") id: String = AccountRepository.getHash()): Response<Message>


    //KVIZTAKEN
    @GET("/student/{id}/kviztaken")
    suspend fun getPokusajeKvizaZaStudenta(@Path("id") id: String = AccountRepository.getHash()): Response<List<KvizTaken>>

    @POST("/student/{id}/kviz/{kid}")
    suspend fun zapocniOdg(@Path("kid") kid: Int, @Path("id") id: String = AccountRepository.getHash()): Response<KvizTaken?>

    //ACCOUNT
    @GET("/student/{id}")
    suspend fun getAccountZaHash(@Path("id") id: String = AccountRepository.getHash()): Response<Account>

    @DELETE("/student/{id}/upisugrupeipokusaji")
    suspend fun deleteAllZaId(@Path("id") id: String = AccountRepository.getHash()): Response<Account>



    //PITANJE
    @GET("/kviz/{id}/pitanja")
    suspend fun getPitanjaNaKvizu(@Path("id") id: Int): Response<List<NovoPitanje>>



}