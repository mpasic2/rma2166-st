package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Account
import java.time.LocalDateTime


@Dao
interface AccountDao {
    @Query("SELECT * FROM Account WHERE acHash=:payload")
    suspend fun getHash(payload:String) : Account

    @Query("DELETE FROM Account")
    suspend fun obrisiAcc()

    @Query("SELECT * FROM Account")
    suspend fun getAccs() : Account

    @Insert
    suspend fun addAcc(acc : Account)



}