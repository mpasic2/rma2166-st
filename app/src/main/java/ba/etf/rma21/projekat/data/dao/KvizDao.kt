package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Account
import ba.etf.rma21.projekat.data.models.Kviz
@Dao
interface KvizDao {

    @Insert
    suspend fun addQuiz(quiz : Kviz)


    @Query("DELETE FROM Kviz")
    suspend fun obrisiKvz()

    @Query("SELECT * FROM Kviz")
    suspend fun getKviz() : List<Kviz>
}