package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Account
import ba.etf.rma21.projekat.data.models.Odgovor
import ba.etf.rma21.projekat.data.models.Pitanje

@Dao
interface OdgovorDao {

    @Insert
    suspend fun addAnswr(answr : Odgovor)

    @Query("DELETE FROM Odgovor")
    suspend fun obrisiOdg()

    @Query("SELECT * FROM Odgovor")
    suspend fun getOdg() : List<Odgovor>


}