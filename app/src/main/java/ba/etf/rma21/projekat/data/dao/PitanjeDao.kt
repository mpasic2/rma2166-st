package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Account
import ba.etf.rma21.projekat.data.models.Pitanje
@Dao
interface PitanjeDao {

    @Insert
    suspend fun addQuestio(quest : Pitanje)

    @Query("DELETE FROM Pitanje")
    suspend fun obrisiPitanje()

    @Query("SELECT * FROM Pitanje")
    suspend fun getPitanja() : List<Pitanje>

}