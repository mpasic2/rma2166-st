/*
package ba.etf.rma21.projekat.data

import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import ba.etf.rma21.projekat.data.repositories.KvizRepository.Companion.listaSvihMojihKvizova
import java.util.*


fun dajDatum(godina:Int,mjesec:Int,dan:Int): Date{
    val kalendar: Calendar
    kalendar = Calendar.getInstance();
    val mj = mjesec-1
    kalendar.set(godina, mj, dan)
    return kalendar.time
}

fun dajSveKivoze(): List<Kviz>{
    val sviKvizovi:MutableList<Kviz> = mutableListOf();
    sviKvizovi.addAll(dajBuduceKvizove())
    sviKvizovi.addAll(dajUradjeneKvizove())
    sviKvizovi.addAll(dajKvizoveNeUradjene())
    //sviKvizovi.addAll(dajSveMojeKvizove())
    //sviKvizovi.addAll(pa funkicja)
    //sviKvizovi.addAll(listaSvihMojihKvizova)


    val listaKonacka: List<Kviz> = sviKvizovi;
    val sortiranaLista = listaKonacka.sortedBy { it.datumPocetka }
    return  sortiranaLista.distinct().toList()
}

fun dajUradjeneKvizove(): List<Kviz>{
    return listOf(
        Kviz(1,"Vjezba 2", dajDatum(2021,4,3), dajDatum(2021,4,3),1,"Grupa 1",2.5f),
            Kviz("Vjezba 1","BWT", dajDatum(2021,3,12), dajDatum(2021,3,12), dajDatum(2021,3,12),20,"Grupa 2",4f),
            Kviz("Vjezba 2","PJP", dajDatum(2021,4,3), dajDatum(2021,4,3), dajDatum(2021,4,3),10,"Grupa 1",3f),
            Kviz("Vjezba 1","OIS", dajDatum(2020,4,3), dajDatum(2020,4,3), dajDatum(2020,4,3),4,"Grupa 1",2.5f),
            Kviz("Vjezba 1","NRS", dajDatum(2021,3,12), dajDatum(2021,3,12), dajDatum(2021,3,12),10,"Grupa 1",0.5f),
            Kviz("Vjezba 2","NRS", dajDatum(2021,4,3), dajDatum(2021,4,3), dajDatum(2021,4,3),3,"Grupa 2",2f),
            Kviz("Vjezba 1","TP", dajDatum(2021,4,4), dajDatum(2021,4,4), dajDatum(2021,4,4),3,"Grupa 1",2f)


    ).sortedBy { it.datumPocetka }
}

fun dajSveMojeKvizove(): List<Kviz>{
    //treba dodat
   return KvizRepository.listaSvihMojihKvizova.sortedBy { it.datumPocetka }

}

fun dajBuduceKvizove(): List<Kviz>{
    return listOf(
            Kviz("Vjezba 2","WT", dajDatum(2021,7,12), dajDatum(2021,7,12), null,10,"Grupa 2",null),
            Kviz("Vjezba 3","RMA", dajDatum(2021,10,3), dajDatum(2021,10,3), null,10,"Grupa 2",null),
            Kviz("Vjezba 3","BWT", dajDatum(2021,5,12), dajDatum(2021,5,12), null,10,"Grupa 1",null),
            Kviz("Vjezba 3","OIS", dajDatum(2021,6,3), dajDatum(2021,6,3), null,10,"Grupa 2",null),
            Kviz("Vjezba 3","PJP", dajDatum(2021,9,12), dajDatum(2021,9,12), null,5,"Grupa 2",null),
            Kviz("Vjezba 2","VVS", dajDatum(2021,5,13), dajDatum(2021,5,13), null,2,"Grupa 1",null),
            Kviz("Vjezba 2","VVS", dajDatum(2021,5,14), dajDatum(2021,5,14), null,2,"Grupa 2",null),
            Kviz("Vjezba 2","RV", dajDatum(2021,5,12), dajDatum(2021,5,12), null,2,"Grupa 1",null),
            Kviz("Vjezba 2","RV", dajDatum(2021,5,13), dajDatum(2021,5,13), null,2,"Grupa 2",null),
            Kviz("Vjezba 2","NOS", dajDatum(2021,5,12), dajDatum(2021,5,12), null,2,"Grupa 2",null),
            Kviz("Vjezba 2","NOS", dajDatum(2021,5,13), dajDatum(2021,5,13), null,2,"Grupa 1",null),
            Kviz("Vjezba 1","MUR2", dajDatum(2021,8,9), dajDatum(2021,8,9), null,3,"Grupa 2",null),
            Kviz("Vjezba 2","TP", dajDatum(2021,3,3), dajDatum(2021,7,3), null,3,"Grupa 2",null)
    ).sortedBy { it.datumPocetka }
}

fun dajKvizoveNeUradjene(): List<Kviz>{
    return listOf(
            Kviz("Vjezba 1","IF1", dajDatum(2020,4,5), dajDatum(2020,4,5), dajDatum(2020,4,5),8,"Grupa 2",null),
            Kviz("Vjezba 1","WT", dajDatum(2020,8,9), dajDatum(2020,8,9), dajDatum(2020,8,9),3,"Grupa 1",null)
            ).sortedBy { it.datumPocetka }
}



fun dajSveKvizoveZaPredmetizaGrupu(predmet: String, grupe: String) : MutableList<Kviz> {
    val dajSvekvizove = dajSveKivoze()
    val praznaLista : MutableList<Kviz> = mutableListOf()
    for(kviz in dajSvekvizove){
            if(predmet==(kviz.nazivPredmeta) && grupe==(kviz.nazivGrupe))
                praznaLista.add(kviz)
    }
    return praznaLista
}
*/
