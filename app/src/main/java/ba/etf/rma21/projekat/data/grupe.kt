/*
package ba.etf.rma21.projekat.data

import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet

var sveUpisaneGrupe = mutableListOf<Grupa>(
        Grupa("Grupa 1","RMA"),
        Grupa("Grupa 2","IF1")
)

fun dajSveGrupe(): List<Grupa> {
    return listOf(
            Grupa("Grupa 1","RMA"),
            Grupa("Grupa 2","RMA"),
            Grupa("Grupa 1","OIS"),
            Grupa("Grupa 2","OIS"),
            Grupa("Grupa 1","IF1"),
            Grupa("Grupa 2","IF1"),
            Grupa("Grupa 1","NRS"),
            Grupa("Grupa 2","NRS"),
            Grupa("Grupa 1","TP"),
            Grupa("Grupa 2","TP"),
            Grupa("Grupa 1","WT"),
            Grupa("Grupa 2","WT"),
            Grupa("Grupa 1","PJP"),
            Grupa("Grupa 2","PJP"),
            Grupa("Grupa 1","VVS"),
            Grupa("Grupa 2","VVS"),
            Grupa("Grupa 1","RV"),
            Grupa("Grupa 2","RV"),
            Grupa("Grupa 1","NOS"),
            Grupa("Grupa 2","NOS"),
            Grupa("Grupa 1","MUR2"),
            Grupa("Grupa 2","MUR2")


    )
}


fun dajSveGrupePoPredmetima(predmet: String): MutableList<Grupa> {
    val sveGrupe = dajSveGrupe()
    val praznaLista: MutableList<Grupa> = mutableListOf()
    for(grupa in sveGrupe){
        if(predmet==(grupa.nazivPredmeta)) {
            praznaLista.add(grupa)
        }
    }

    return praznaLista
}
*/
