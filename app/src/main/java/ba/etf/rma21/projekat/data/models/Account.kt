package ba.etf.rma21.projekat.data.models

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*
@Entity
data class Account(
    @PrimaryKey()
    @SerializedName("id") val id: Int,

    @ColumnInfo(name = "acHash")
    @SerializedName("acHash") val acHash : String,

    @ColumnInfo(name = "lastUpdate")
    @SerializedName("lastUpdate") val lastUpdate : String) {



}