package ba.etf.rma21.projekat.data.models

import com.google.gson.annotations.SerializedName

data class ChangedObject (
    @SerializedName("changed") val changed : Boolean ){


}