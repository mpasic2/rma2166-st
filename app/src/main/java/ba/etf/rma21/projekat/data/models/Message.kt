package ba.etf.rma21.projekat.data.models

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.SerializedName
@Entity
data class Message (
    @ColumnInfo(name = "poruka")
    @SerializedName("poruka") val poruka:String) {


}
