package ba.etf.rma21.projekat.data.models;

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import com.google.gson.annotations.SerializedName;


@Entity
data class PostaviOdg(
    @ColumnInfo(name = "odgovor")
    @SerializedName("odgovor") val odgovor : Int,

    @ColumnInfo(name = "pitanje")
    @SerializedName("pitanje") val pitanje : Int,

    @ColumnInfo(name = "bodovi")
    @SerializedName("bodovi") val bodovi : Int)