/*
package ba.etf.rma21.projekat.data

import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.models.PitanjeKviz

var dajSvaPitanja = mutableListOf<Pitanje>(

        Pitanje(1,"Test1","Tacan odg je b", listOf("a","b","c"),1),
        Pitanje(2,"Test2","Tacan odg je c", listOf("a","b","c"),2),
        Pitanje(3,"Test3","Jel tezak RMA", listOf("2","4","6"),0),
    Pitanje(4,"Sabiranje","Koliko je 2 + 2", listOf("2","4","5"),1),
    Pitanje(5,"Oduzimanje","Koliko je 2 - 2", listOf("0","1","Ne znam"),0),
    Pitanje(6,"Mnozenje","Koliko je 2 puta 2", listOf("2","4","6"),1)


)


var dajPitanjeKvizSvaPitanja = mutableListOf<PitanjeKviz>(

        PitanjeKviz("Sabiranje", Kviz("Vjezba 2","MUR2", dajDatum(2021,4,3), dajDatum(2021,4,3), dajDatum(2021,4,3),1,"Grupa 1",2.5f)),
        PitanjeKviz("Oduzimanje", Kviz("Vjezba 2","MUR2", dajDatum(2021,4,3), dajDatum(2021,4,3), dajDatum(2021,4,3),1,"Grupa 1",2.5f)),
        PitanjeKviz("Mnozenje", Kviz("Vjezba 2","MUR2", dajDatum(2021,4,3), dajDatum(2021,4,3), dajDatum(2021,4,3),1,"Grupa 1",2.5f)),


        PitanjeKviz("Sabiranje", Kviz("Vjezba 1","BWT", dajDatum(2021,3,12), dajDatum(2021,3,12), dajDatum(2021,3,12),20,"Grupa 2",4f)),
        PitanjeKviz("Oduzimanje", Kviz("Vjezba 1","BWT", dajDatum(2021,3,12), dajDatum(2021,3,12), dajDatum(2021,3,12),20,"Grupa 2",4f)),
        PitanjeKviz("Mnozenje", Kviz("Vjezba 1","BWT", dajDatum(2021,3,12), dajDatum(2021,3,12), dajDatum(2021,3,12),20,"Grupa 2",4f)),



        PitanjeKviz("Mnozenje", Kviz("Vjezba 2","PJP", dajDatum(2021,4,3), dajDatum(2021,4,3), dajDatum(2021,4,3),10,"Grupa 1",3f)),
        PitanjeKviz("Oduzimanje", Kviz("Vjezba 2","PJP", dajDatum(2021,4,3), dajDatum(2021,4,3), dajDatum(2021,4,3),10,"Grupa 1",3f)),
        PitanjeKviz("Sabiranje", Kviz("Vjezba 2","PJP", dajDatum(2021,4,3), dajDatum(2021,4,3), dajDatum(2021,4,3),10,"Grupa 1",3f)),



        PitanjeKviz("Mnozenje",             Kviz("Vjezba 1","OIS", dajDatum(2020,4,3), dajDatum(2020,4,3), dajDatum(2020,4,3),4,"Grupa 1",2.5f)),
        PitanjeKviz("Oduzimanje",             Kviz("Vjezba 1","OIS", dajDatum(2020,4,3), dajDatum(2020,4,3), dajDatum(2020,4,3),4,"Grupa 1",2.5f)),
        PitanjeKviz("Sabiranje",             Kviz("Vjezba 1","OIS", dajDatum(2020,4,3), dajDatum(2020,4,3), dajDatum(2020,4,3),4,"Grupa 1",2.5f)),



        PitanjeKviz("Mnozenje",Kviz("Vjezba 1","NRS", dajDatum(2021,3,12), dajDatum(2021,3,12), dajDatum(2021,3,12),10,"Grupa 1",0.5f)),
        PitanjeKviz("Oduzimanje",            Kviz("Vjezba 1","NRS", dajDatum(2021,3,12), dajDatum(2021,3,12), dajDatum(2021,3,12),10,"Grupa 1",0.5f)),
        PitanjeKviz("Sabiranje",            Kviz("Vjezba 1","NRS", dajDatum(2021,3,12), dajDatum(2021,3,12), dajDatum(2021,3,12),10,"Grupa 1",0.5f)),




        PitanjeKviz("Mnozenje",             Kviz("Vjezba 2","NRS", dajDatum(2021,4,3), dajDatum(2021,4,3), dajDatum(2021,4,3),3,"Grupa 2",2f)),
        PitanjeKviz("Oduzimanje",             Kviz("Vjezba 2","NRS", dajDatum(2021,4,3), dajDatum(2021,4,3), dajDatum(2021,4,3),3,"Grupa 2",2f)),
        PitanjeKviz("Sabiranje",             Kviz("Vjezba 2","NRS", dajDatum(2021,4,3), dajDatum(2021,4,3), dajDatum(2021,4,3),3,"Grupa 2",2f)),




        PitanjeKviz("Mnozenje",            Kviz("Vjezba 1","TP", dajDatum(2021,4,4), dajDatum(2021,4,4), dajDatum(2021,4,4),3,"Grupa 1",2f)),
        PitanjeKviz("Oduzimanje",             Kviz("Vjezba 1","TP", dajDatum(2021,4,4), dajDatum(2021,4,4), dajDatum(2021,4,4),3,"Grupa 1",2f)),
        PitanjeKviz("Sabiranje",             Kviz("Vjezba 1","TP", dajDatum(2021,4,4), dajDatum(2021,4,4), dajDatum(2021,4,4),3,"Grupa 1",2f)),




        PitanjeKviz("Mnozenje",                        Kviz("Vjezba 2","WT", dajDatum(2021,7,12), dajDatum(2021,7,12), null,10,"Grupa 2",null)),
        PitanjeKviz("Oduzimanje",                         Kviz("Vjezba 2","WT", dajDatum(2021,7,12), dajDatum(2021,7,12), null,10,"Grupa 2",null)),
        PitanjeKviz("Sabiranje",                        Kviz("Vjezba 2","WT", dajDatum(2021,7,12), dajDatum(2021,7,12), null,10,"Grupa 2",null)),





        PitanjeKviz("Mnozenje",             Kviz("Vjezba 3","RMA", dajDatum(2021,10,3), dajDatum(2021,10,3), null,10,"Grupa 2",null)),
        PitanjeKviz("Oduzimanje",             Kviz("Vjezba 3","RMA", dajDatum(2021,10,3), dajDatum(2021,10,3), null,10,"Grupa 2",null)),
        PitanjeKviz("Sabiranje",             Kviz("Vjezba 3","RMA", dajDatum(2021,10,3), dajDatum(2021,10,3), null,10,"Grupa 2",null)),




        PitanjeKviz("Mnozenje",            Kviz("Vjezba 3","BWT", dajDatum(2021,5,12), dajDatum(2021,5,12), null,10,"Grupa 1",null)),
        PitanjeKviz("Oduzimanje",             Kviz("Vjezba 3","BWT", dajDatum(2021,5,12), dajDatum(2021,5,12), null,10,"Grupa 1",null)),
        PitanjeKviz("Sabiranje",             Kviz("Vjezba 3","BWT", dajDatum(2021,5,12), dajDatum(2021,5,12), null,10,"Grupa 1",null)),




        PitanjeKviz("Mnozenje",             Kviz("Vjezba 3","OIS", dajDatum(2021,6,3), dajDatum(2021,6,3), null,10,"Grupa 2",null)),
        PitanjeKviz("Oduzimanje",             Kviz("Vjezba 3","OIS", dajDatum(2021,6,3), dajDatum(2021,6,3), null,10,"Grupa 2",null)),
        PitanjeKviz("Sabiranje",             Kviz("Vjezba 3","OIS", dajDatum(2021,6,3), dajDatum(2021,6,3), null,10,"Grupa 2",null)),




        PitanjeKviz("Mnozenje",             Kviz("Vjezba 3","PJP", dajDatum(2021,9,12), dajDatum(2021,9,12), null,5,"Grupa 2",null)),
        PitanjeKviz("Oduzimanje",             Kviz("Vjezba 3","PJP", dajDatum(2021,9,12), dajDatum(2021,9,12), null,5,"Grupa 2",null)),
        PitanjeKviz("Sabiranje",             Kviz("Vjezba 3","PJP", dajDatum(2021,9,12), dajDatum(2021,9,12), null,5,"Grupa 2",null)),




        PitanjeKviz("Mnozenje",             Kviz("Vjezba 2","VVS", dajDatum(2021,5,13), dajDatum(2021,5,13), null,2,"Grupa 1",null)),
        PitanjeKviz("Oduzimanje",             Kviz("Vjezba 2","VVS", dajDatum(2021,5,13), dajDatum(2021,5,13), null,2,"Grupa 1",null)),
        PitanjeKviz("Sabiranje",             Kviz("Vjezba 2","VVS", dajDatum(2021,5,13), dajDatum(2021,5,13), null,2,"Grupa 1",null)),




        PitanjeKviz("Mnozenje",             Kviz("Vjezba 2","VVS", dajDatum(2021,5,14), dajDatum(2021,5,14), null,2,"Grupa 2",null)),
        PitanjeKviz("Oduzimanje",             Kviz("Vjezba 2","VVS", dajDatum(2021,5,14), dajDatum(2021,5,14), null,2,"Grupa 2",null)),
        PitanjeKviz("Sabiranje",             Kviz("Vjezba 2","VVS", dajDatum(2021,5,14), dajDatum(2021,5,14), null,2,"Grupa 2",null)),




        PitanjeKviz("Mnozenje",             Kviz("Vjezba 2","RV", dajDatum(2021,5,12), dajDatum(2021,5,12), null,2,"Grupa 1",null)),
        PitanjeKviz("Oduzimanje",            Kviz("Vjezba 2","RV", dajDatum(2021,5,12), dajDatum(2021,5,12), null,2,"Grupa 1",null)),
        PitanjeKviz("Sabiranje",          Kviz("Vjezba 2","RV", dajDatum(2021,5,12), dajDatum(2021,5,12), null,2,"Grupa 1",null)),



        PitanjeKviz("Mnozenje",             Kviz("Vjezba 2","RV", dajDatum(2021,5,13), dajDatum(2021,5,13), null,2,"Grupa 2",null)),
        PitanjeKviz("Oduzimanje",             Kviz("Vjezba 2","RV", dajDatum(2021,5,13), dajDatum(2021,5,13), null,2,"Grupa 2",null)),
        PitanjeKviz("Sabiranje",             Kviz("Vjezba 2","RV", dajDatum(2021,5,13), dajDatum(2021,5,13), null,2,"Grupa 2",null)),



        PitanjeKviz("Mnozenje",             Kviz("Vjezba 2","NOS", dajDatum(2021,5,12), dajDatum(2021,5,12), null,2,"Grupa 2",null)),
        PitanjeKviz("Oduzimanje",             Kviz("Vjezba 2","NOS", dajDatum(2021,5,12), dajDatum(2021,5,12), null,2,"Grupa 2",null)),
        PitanjeKviz("Sabiranje",             Kviz("Vjezba 2","NOS", dajDatum(2021,5,12), dajDatum(2021,5,12), null,2,"Grupa 2",null)),




        PitanjeKviz("Mnozenje",             Kviz("Vjezba 2","NOS", dajDatum(2021,5,13), dajDatum(2021,5,13), null,2,"Grupa 1",null)),
        PitanjeKviz("Oduzimanje",             Kviz("Vjezba 2","NOS", dajDatum(2021,5,13), dajDatum(2021,5,13), null,2,"Grupa 1",null)),
        PitanjeKviz("Sabiranje",             Kviz("Vjezba 2","NOS", dajDatum(2021,5,13), dajDatum(2021,5,13), null,2,"Grupa 1",null)),



        PitanjeKviz("Mnozenje",             Kviz("Vjezba 1","MUR2", dajDatum(2021,8,9), dajDatum(2021,8,9), null,3,"Grupa 2",null)),
        PitanjeKviz("Oduzimanje",             Kviz("Vjezba 1","MUR2", dajDatum(2021,8,9), dajDatum(2021,8,9), null,3,"Grupa 2",null)),
        PitanjeKviz("Sabiranje",             Kviz("Vjezba 1","MUR2", dajDatum(2021,8,9), dajDatum(2021,8,9), null,3,"Grupa 2",null)),


        PitanjeKviz("Mnozenje",             Kviz("Vjezba 2","TP", dajDatum(2021,3,3), dajDatum(2021,7,3), null,3,"Grupa 2",null)),
        PitanjeKviz("Oduzimanje",             Kviz("Vjezba 2","TP", dajDatum(2021,3,3), dajDatum(2021,7,3), null,3,"Grupa 2",null)),
        PitanjeKviz("Sabiranje",             Kviz("Vjezba 2","TP", dajDatum(2021,3,3), dajDatum(2021,7,3), null,3,"Grupa 2",null)),


        PitanjeKviz("Mnozenje",                        Kviz("Vjezba 1","IF1", dajDatum(2020,4,5), dajDatum(2020,4,5), dajDatum(2020,4,5),8,"Grupa 2",null)),
        PitanjeKviz("Oduzimanje",                        Kviz("Vjezba 1","IF1", dajDatum(2020,4,5), dajDatum(2020,4,5), dajDatum(2020,4,5),8,"Grupa 2",null)),
        PitanjeKviz("Sabiranje",                        Kviz("Vjezba 1","IF1", dajDatum(2020,4,5), dajDatum(2020,4,5), dajDatum(2020,4,5),8,"Grupa 2",null)),



        PitanjeKviz("Mnozenje",             Kviz("Vjezba 1","WT", dajDatum(2020,8,9), dajDatum(2020,8,9), dajDatum(2020,8,9),3,"Grupa 1",null)),
        PitanjeKviz("Oduzimanje",            Kviz("Vjezba 1","WT", dajDatum(2020,8,9), dajDatum(2020,8,9), dajDatum(2020,8,9),3,"Grupa 1",null)),
        PitanjeKviz("Sabiranje",             Kviz("Vjezba 1","WT", dajDatum(2020,8,9), dajDatum(2020,8,9), dajDatum(2020,8,9),3,"Grupa 1",null)),


        PitanjeKviz("Mnozenje",             Kviz("Vjezba 2","IF1", dajDatum(2021,5,12), dajDatum(2021,5,12), null,2,"Grupa 1",null)),
        PitanjeKviz("Oduzimanje",            Kviz("Vjezba 2","IF1", dajDatum(2021,5,12), dajDatum(2021,5,12), null,2,"Grupa 1",null)),
        PitanjeKviz("Sabiranje",             Kviz("Vjezba 2","IF1", dajDatum(2021,5,12), dajDatum(2021,5,12), null,2,"Grupa 1",null)),



        PitanjeKviz("Test1",             Kviz("Vjezba 2","RMA", dajDatum(2021,2,12), dajDatum(2021,2,12), dajDatum(2021,2,12),6,"Grupa 1",1f)),
        PitanjeKviz("Test2",            Kviz("Vjezba 2","RMA", dajDatum(2021,2,12), dajDatum(2021,2,12), dajDatum(2021,2,12),6,"Grupa 1",1f)),
        PitanjeKviz("Test3",             Kviz("Vjezba 2","RMA", dajDatum(2021,2,12), dajDatum(2021,2,12), dajDatum(2021,2,12),6,"Grupa 1",1f))






)
*/
