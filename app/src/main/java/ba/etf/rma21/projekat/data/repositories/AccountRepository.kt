package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.dao.AccountDao
import ba.etf.rma21.projekat.data.models.Account
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*


class AccountRepository {

    companion object {
        //TODO Ovdje trebate dodati hash string vašeg accounta
        var acHash: String = "00fefa88-61dc-42bd-b109-6cacc65b1153"
        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }

        @RequiresApi(Build.VERSION_CODES.O)
        suspend fun postaviHash(acHash: String): Boolean {
            this.acHash = acHash
            return withContext(Dispatchers.IO) {
                val baza = AppDatabase.getInstance(context)


                //brisanje
                baza.accountDao().obrisiAcc()
                baza.grupaDao().obrisiGrp()
                baza.kvizDao().obrisiKvz()
                baza.odgovorDao().obrisiOdg(); baza.pitanjeDao().obrisiPitanje(); baza.predmetDao().obrisiSubject();
                val vrijme = LocalDateTime.now().minusSeconds(2).format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"))
                val acc = Account(1, acHash, vrijme)
                baza.accountDao().addAcc(acc)
                DBRepository.updateNow()
                return@withContext true
            }
        }

        fun getHash(): String {
            return acHash
        }


    }
}