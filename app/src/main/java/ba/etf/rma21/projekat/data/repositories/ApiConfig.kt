package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.Api
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ba.etf.rma21.projekat.data.repositories.ApiConfig

class ApiConfig {

    companion object{
        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }
        var baseURL: String = "https://rma21-etf.herokuapp.com"

        fun postaviBaseURL(baseUrl:String):Unit{
            baseURL=baseUrl
        }
    }
}