package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import ba.etf.rma21.projekat.data.ApiAdapter
import ba.etf.rma21.projekat.data.AppDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONObject
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class DBRepository {
    companion object{
        var time = 0
        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }

        @RequiresApi(Build.VERSION_CODES.O)
        suspend fun updateNow():Boolean{
            return withContext(Dispatchers.IO) {

                val baza = AppDatabase.getInstance(context)
                val dtime = baza.accountDao().getAccs()


                val response = ApiAdapter.retrofit.accZadnjiUpdate(dtime.acHash,dtime.lastUpdate)
                val responseBody = response.body()




                if (responseBody != null) {
                    return@withContext responseBody.changed
                }
                else
                    return@withContext false
            }
        }
    }
}