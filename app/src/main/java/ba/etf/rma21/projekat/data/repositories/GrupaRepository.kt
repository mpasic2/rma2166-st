package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.ApiAdapter
import ba.etf.rma21.projekat.data.models.Grupa
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.withContext

class GrupaRepository {
    companion object {
        init {
            // TODO: Implementirati
        }
        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }
        var predmeti : List<String> = mutableListOf()
        suspend fun getGroupsByPredmet(nazivPredmeta: String): List<Grupa> {
            val sviPredmeti = PredmetRepository.getAll()
            for(predm in sviPredmeti!!){

                if(nazivPredmeta==predm.naziv)
                    return this.getAllGroupsForId(predm.id)!!
            }
            return emptyList()

        }

        suspend fun getAllGroupsForId(id : Int): List<Grupa>? {
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.getAllGrupeNaPredmetuZaId(id)
                val responseBody = response.body()
                return@withContext responseBody
            }

        }


        suspend fun getGrupePoKvizu(id : Int): List<Grupa>? {
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.getGrupeUkojimaJeKvizDostupan(id)
                val responseBody = response.body()
                return@withContext responseBody
            }

        }

        suspend fun getAllGroups(): List<Grupa>? {
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.getSveGrupe()
                val responseBody = response.body()
                return@withContext responseBody
            }

        }
    }
}