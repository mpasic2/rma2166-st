package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.*
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Kviz
import kotlinx.coroutines.*
import java.net.MalformedURLException
import java.util.*

class KvizRepository {

    companion object {

        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }

        fun dajDatum(datum : String?): Date{
            val godina:Int
            val mjesec:Int
            val dan:Int

            val ddd = datum?.split("T")?.get(0)
            if(ddd==null) return Date()
            godina = Integer.parseInt(ddd?.split("-")?.get(0))
            mjesec = Integer.parseInt(ddd?.split("-")?.get(1))
            dan = Integer.parseInt(ddd?.split("-")?.get(2))


            val kalendar: Calendar
            kalendar = Calendar.getInstance();
            val mj = mjesec-1
            kalendar.set(godina, mj, dan)
            return kalendar.time
        }




        init {
            // TODO: Implementirati
        }

        suspend fun getMyKvizes(): List<Kviz> {
            //return this.getUpisani()!!
            //izmjenjeno
            var baza = AppDatabase.getInstance(context)
            return baza.kvizDao().getKviz()

        }

        suspend fun getAl1l(): List<Kviz> {
            return this.getAll()!!
        }


        suspend fun getDone(): List<Kviz> {
            //return this.getAll()!!.filter { x -> (x.datumKraj!=null || x.datumKraj!!.before(Date())) }.sortedBy { x -> x.datumPocetka }
            val sviKvizovi = getMyKvizes()
            val sviTakenKvizovi = TakeKvizRepository.getPocetiKvizovi()
            val listaDoneKvizova : MutableList<Kviz> = mutableListOf()
            for(kviz1 in sviKvizovi)
                for(kviz2 in sviTakenKvizovi!!)
                    if(kviz2.KvizId==kviz1.id)
                        listaDoneKvizova.add(kviz1)
            return listaDoneKvizova
        }

        suspend fun getFuture(): List<Kviz> {
            return this.getUpisani()!!.filter { x -> (x.datumKraj==null || dajDatum(x.datumKraj)!!.after(Date())) }
        }


        suspend fun getNotTaken(): List<Kviz> {
            var sviPoceti = TakeKvizRepository.getPocetiKvizovi()
            var sviKvizovi = getUpisani()
            var lista : MutableList<Kviz> = mutableListOf()

            if(sviKvizovi.isNullOrEmpty() || sviPoceti.isNullOrEmpty()) return emptyList()

            for(i in sviKvizovi!!)
                for(j in sviPoceti!!)
                    if(i.id==j.KvizId && dajDatum(i.datumKraj)!!.after(dajDatum(i.datumPocetka)))
                        lista.add(i)

            return lista
        }



        suspend fun getAll(): List<Kviz>? {

                return withContext(Dispatchers.IO) {
                    val listPrazna :List<Kviz> = emptyList()

                    val response = ApiAdapter.retrofit.getSveKvizove()
                    val responseBody = response.body()
                    if(responseBody.isNullOrEmpty()) return@withContext listPrazna
                    else return@withContext responseBody

            }
        }

        suspend fun getById(idKviza: Int): Kviz? {
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.getKvizZaId(idKviza)
                val responseBody = response.body()
                return@withContext responseBody
            }
        }


        suspend fun getUpisani(): List<Kviz>? {
            val sviUpisaniKvizovi : MutableList<Kviz> = mutableListOf()
            val sveMoguceGrupe : MutableList<Grupa> = mutableListOf()
            val grupeOdStudenta : List<Grupa>? = ApiAdapter.retrofit.getGroupWithStudent().body()//Predmet.rep.getUpisane
            if(grupeOdStudenta!=null)
                for(grupice in grupeOdStudenta) {
                    val sviKvizoviZaGrupe = ApiAdapter.retrofit.getKvizoviZaGrupu(grupice.id).body()
                    sviUpisaniKvizovi.addAll(sviKvizoviZaGrupe!!)
                }
            return sviUpisaniKvizovi


        }


    }
}