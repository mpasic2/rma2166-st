package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.ApiAdapter
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


class OdgovorRepository {
    companion object {
        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }

        suspend fun getOdgovoriKviz(idKviza: Int): List<Odgovor>? {
            return withContext(Dispatchers.IO) {
                val kvizTkn : List<KvizTaken> = TakeKvizRepository.getPocetiKvizovi()!!.filter { kvit -> kvit.KvizId == idKviza }

                    val response = ApiAdapter.retrofit.getOdgovoriZaKviz(kvizTkn[0].id)
                    val responseBody = response.body()
                    return@withContext responseBody

            }
        }

        suspend fun postaviOdgovorKviz(idKvizTaken: Int, idPitanje: Int, odgovor: Int): Int? {

            return withContext(Dispatchers.IO) {
                val kvizoviTaken: List<KvizTaken> = TakeKvizRepository.getPocetiKvizovi()!!
                val kvizTaken : KvizTaken = kvizoviTaken.filter { kvit -> kvit.id == idKvizTaken }[0]
                var sviDosadasnjiBodovi : Int = kvizTaken.osvojeniBodovi
                val svaPitanja : List<NovoPitanje>? = PitanjeKvizRepository.getPitanja(kvizTaken.KvizId)
                val pitanjePoId : NovoPitanje = svaPitanja!!.filter { pitanje -> pitanje.id == idPitanje }[0]
                val baza = AppDatabase.getInstance(context)

                if(odgovor==pitanjePoId.tacan){

                    var postotak = 100/svaPitanja.size
                    sviDosadasnjiBodovi+=Integer.parseInt(postotak.toString())
                }
                var postaviOdg = PostaviOdg(odgovor,idPitanje,sviDosadasnjiBodovi)
                val odgreno = Odgovor(1,odgovor,kvizTaken.id,pitanjePoId.id)
                var br = 1

                for(odgovorena in baza.odgovorDao().getOdg()){
                    if(odgovorena==odgreno) return@withContext sviDosadasnjiBodovi

                }


                //ApiAdapter.retrofit.addAnswerForTry(idKvizTaken,postaviOdg)
                baza.odgovorDao().addAnswr(odgreno)

                return@withContext sviDosadasnjiBodovi
            }

        }



    }


}