package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.ApiAdapter
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.NovoPitanje
import ba.etf.rma21.projekat.data.models.PamcenjeOdgovora
import ba.etf.rma21.projekat.data.models.Pitanje
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PitanjeKvizRepository{

    companion object {
        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }
        val listaOdogovrenih : MutableList<PamcenjeOdgovora> = mutableListOf()

        suspend fun getPitanja(idKviza: Int): List<NovoPitanje>? {
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.getPitanjaNaKvizu(idKviza)
                val responseBody = response.body()
                return@withContext responseBody
            }
        }

        suspend fun daj(idKviza: Int): List<Pitanje>? {
            return AppDatabase.getInstance(context).pitanjeDao().getPitanja()
        }






    }


}