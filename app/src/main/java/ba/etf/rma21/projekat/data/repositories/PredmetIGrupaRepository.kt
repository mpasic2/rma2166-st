package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.ApiAdapter
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


class PredmetIGrupaRepository {
    companion object{
        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }

        suspend fun getPredmeti():List<Predmet>? {
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.getSviPredmeti()
                val responseBody = response.body()
                return@withContext responseBody
            }
        }
        suspend fun getGrupe():List<Grupa>?{
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.getSveGrupe()
                val responseBody = response.body()
                return@withContext responseBody
            }
        }

        suspend fun getGrupeZaPredmet(idPredmeta:Int):List<Grupa>?{
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.getAllGrupeNaPredmetuZaId(idPredmeta)
                val responseBody = response.body()
                return@withContext responseBody
            }
        }

        suspend fun upisiUGrupu(idGrupa:Int):Boolean?{
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.addStudentWithHash(idGrupa)
                println("ovo je response.message" + response.message())
                println("ovo je response.body.poruka" + response.body()?.poruka)
                //return@withContext response.body()?.poruka?.contains("je dodan")
                return@withContext response.message().equals("OK")
            }

        }

        suspend fun getUpisaneGrupe():List<Grupa>?{
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.getGroupWithStudent()
                val responseBody = response.body()
                return@withContext responseBody
            }
        }


        suspend fun getPredmetZaId(id: Int):Predmet?{
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.getPredmetZaId(id)
                val responseBody = response.body()
                return@withContext responseBody
            }
        }


    }
}