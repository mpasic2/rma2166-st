package ba.etf.rma21.projekat.data.repositories


import android.content.Context
import ba.etf.rma21.projekat.data.ApiAdapter
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Predmet
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.withContext

class PredmetRepository {


    companion object {
        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }


        suspend fun getUpisani(): List<Predmet> {

            val sveGrupePoPredmetimaOdStudenta = PredmetIGrupaRepository.getUpisaneGrupe()
            val sviPredmetiLista = PredmetRepository.getAll()
            val listaPredmetaZaVracanje : MutableList<Predmet> = mutableListOf()

            for(grupa in sveGrupePoPredmetimaOdStudenta!!){
                for(predmet in sviPredmetiLista!!) {
                    if (grupa.PredmetId == predmet.id)
                        listaPredmetaZaVracanje.add(predmet)
                }
            }
        return listaPredmetaZaVracanje
        }

        suspend fun getAll(): List<Predmet>? {
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.getSviPredmeti()
                val responseBody = response.body()
                return@withContext responseBody
            }
        }


        suspend fun dajPredmetZaKviz(idKviza : Int): String {
            val grupePredmeta = GrupaRepository.getGrupePoKvizu(idKviza)

            var listaPredmeta : String = ""
            for(grupa in grupePredmeta!!){
                val predm : Predmet = PredmetIGrupaRepository.getPredmetZaId(grupa.PredmetId)!!
                listaPredmeta+=predm.naziv
            }
            return listaPredmeta
        }


    }

}