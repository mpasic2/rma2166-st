package ba.etf.rma21.projekat

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository
import ba.etf.rma21.projekat.view.FragmentPokusaj
import ba.etf.rma21.projekat.view.QuizListAdapter
import ba.etf.rma21.projekat.viewModel.QuizListViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class FragmentKvizovi : Fragment(), AdapterView.OnItemSelectedListener {


    private lateinit var listaKvizova: RecyclerView
    private lateinit var quizListAdapter: QuizListAdapter
    private var quizListViewModel   =  QuizListViewModel()
    private lateinit var bottomNav: BottomNavigationView


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) : View? {
        var view:View = inflater.inflate(R.layout.quiz_activity, container, false)

        //dodjeljujemo vrijednost
        listaKvizova = view.findViewById(R.id.listaKvizova)
        listaKvizova = view.findViewById(R.id.listaKvizova)
        listaKvizova.layoutManager = GridLayoutManager(view.context, 2)
        bottomNav = activity!!.findViewById(R.id.bottomNav)


        bottomNav.menu.findItem(R.id.kvizovi).setVisible(true)
        bottomNav.menu.findItem(R.id.predmeti).setVisible(true)

        bottomNav.menu.findItem(R.id.predajKviz).setVisible(false)
        bottomNav.menu.findItem(R.id.zaustaviKviz).setVisible(false)

        GlobalScope.launch(Dispatchers.Main) {
            quizListAdapter = QuizListAdapter(listOf()) { kviz -> openFragment(kviz)}
            listaKvizova.adapter = quizListAdapter
            quizListAdapter.azurirajKvizove(quizListViewModel.dajKvizoveSve())
        }

        val filterKvizova : Spinner = view.findViewById(R.id.filterKvizova)
        ArrayAdapter.createFromResource(view.context, R.array.dropDownFilteri,android.R.layout.simple_spinner_dropdown_item).also {
                adap -> adap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            filterKvizova.adapter = adap
        }

        filterKvizova.onItemSelectedListener=this

        /*upisDugme = findViewById(R.id.upisDugme)
        upisDugme.setOnClickListener {
            val intnt = Intent(view.context, UpisPredmet::class.java)
            startActivity(intnt)
        }*/
        return view
    }

    companion object {
        fun newInstance(): FragmentKvizovi = FragmentKvizovi()
        lateinit var kviz1 : Kviz

    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        //za sve
        GlobalScope.launch(Dispatchers.Main) {
            if (position == 0) quizListAdapter.azurirajKvizove(quizListViewModel.dajSveMojeKvizove())
            else if (position == 1) quizListAdapter.azurirajKvizove(quizListViewModel.dajKvizoveSve())
            else if (position == 2) quizListAdapter.azurirajKvizove(quizListViewModel.dajUrajdeneSve())
            else if (position == 3) quizListAdapter.azurirajKvizove(quizListViewModel.dajBuduceKvizove())
            else quizListAdapter.azurirajKvizove(quizListViewModel.dajNeUradjeneKvizove())
        }
    }




    private fun openFragment(kviz: Kviz) {


        GlobalScope.launch(Dispatchers.Main) {
            kviz1 = kviz
            bottomNav.menu.findItem(R.id.kvizovi).setVisible(false)
            bottomNav.menu.findItem(R.id.predmeti).setVisible(false)

            bottomNav.menu.findItem(R.id.predajKviz).setVisible(true)
            bottomNav.menu.findItem(R.id.zaustaviKviz).setVisible(true)

            PitanjeKvizRepository.daj(kviz.id)?.let { FragmentPokusaj.newInstance(it) }?.let {
                activity!!.supportFragmentManager.beginTransaction().replace(
                    R.id.container,
                    it
                ).addToBackStack(null).commit()
            }

        }

    }

}

