package ba.etf.rma21.projekat.view

import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import androidx.core.view.get
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.FragmentKvizovi
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.models.PamcenjeOdgovora
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.repositories.GrupaRepository
import ba.etf.rma21.projekat.data.repositories.OdgovorRepository
import ba.etf.rma21.projekat.data.repositories.PitanjeKvizRepository
import ba.etf.rma21.projekat.data.repositories.TakeKvizRepository
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class FragmentPitanje(var pitanje: Pitanje?) : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) : View? {
        var view: View = inflater.inflate(R.layout.pitanje_fragment, container, false)
        val textviv : TextView
        val lista : ListView
        var navBar : NavigationView


        textviv = view.findViewById(R.id.tekstPitanja)
        lista = view.findViewById(R.id.odgovoriLista)
        //ovo ne radi ne znam sto
        //println("ovo je parent: " + parentFragment)
        navBar = parentFragment?.view!!.findViewById(R.id.navigacijaPitanja)


        textviv.text = pitanje!!.tekstPitanja
        val adapter = ArrayAdapter(view.context,android.R.layout.simple_list_item_1, pitanje!!.opcije!!.split(","))
        lista.adapter = adapter

        lista.post{
            println("ovo se desi cim se prikaze pitanje")


        }

        lista.setOnItemClickListener { parent, view, position, id ->
            GlobalScope.launch(Dispatchers.Main) {
                val pitanje1 = arguments!!.getString("kljucPitanja")
/*                println("ovo je pitanje1: " + pitanje1)
                println("ovo je pozicija1: " + pozicija1)
                println("ovo je pitanje iz fragmenta" + pitanje)*/

                lista.isEnabled = false
                pozicija1 = position
                val txt = view!!.findViewById<TextView>(android.R.id.text1)
                var redniBrPitanja = pitanje1!!.toInt()
                if(pitanje!!.tacan== position) {
                    txt.setTextColor(Color.parseColor("#3DDC84"))
                    val spanString = SpannableString(navBar.menu.getItem(redniBrPitanja.toInt()).title.toString())
                    spanString.setSpan(
                            ForegroundColorSpan(Color.parseColor("#3DDC84")), 0, spanString.length, 0)
                    navBar.menu.getItem(redniBrPitanja.toInt()).title = spanString
                }

                else {
                    txt.setTextColor(Color.parseColor("#DB4F3D"))
                    val spanString = SpannableString(navBar.menu.getItem(redniBrPitanja.toInt()).title.toString())
                    spanString.setSpan(
                            ForegroundColorSpan(Color.parseColor("#DB4F3D")),
                            0,
                            spanString.length,
                            0
                    )
                    navBar.menu.getItem(redniBrPitanja.toInt()).title=spanString
                    lista[pitanje!!.tacan].setBackgroundColor(Color.parseColor("#3DDC84"))
                }

                //OdgovorRepository.postaviOdgovorKviz(1, pitanje!!.id,txt.id)
            }
        }



        return view
    }


    companion object {
        lateinit  var pit : Pitanje
        var pozicija1 = -1

        suspend fun newInstance(br: Int): FragmentPitanje = FragmentPitanje(
            PitanjeKvizRepository.daj(br)?.get(pozicija1))

        fun newnewInstance(): FragmentPitanje = newnewInstance()

    }
}

