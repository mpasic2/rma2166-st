package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.NovoPitanje
import ba.etf.rma21.projekat.data.models.Pitanje
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class FragmentPokusaj(listaPitanja: List<Pitanje>) : Fragment( ) {

    private lateinit var navigacijaLijevo : NavigationView
    private lateinit var frejm : FrameLayout
    var svaPitanjaLista : List<Pitanje> = listaPitanja





    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) : View? {

        var view: View = inflater.inflate(R.layout.pokusaj_fragment_layout, container, false)
        var navigationView : NavigationView
        navigationView=view.findViewById(R.id.navigacijaPitanja)


        for(br in (svaPitanjaLista.indices)){
            var broj = br
            broj += 1
            navigationView.menu.add(1,br,br,broj.toString())
        }

        //navigationView.menu.get(0)
        navigationView.setNavigationItemSelectedListener {
                item ->
            //item.itemId sadrzi redni broj od 1 pa nadalje
            val bunde = Bundle()
            bunde.putString("kljucPitanja",item.itemId.toString())
            otvoriFragmentPitanja(item.itemId,bunde)



            false
        }
        //navigationView.menu.get(0).setChecked(true)

        return view

    }



    companion object {
        fun newInstance(listaPitanja: List<Pitanje>): FragmentPokusaj = FragmentPokusaj(listaPitanja)
    }


    private fun otvoriFragmentPitanja(brojPitanja: Int, bundle:Bundle) {
        GlobalScope.launch(Dispatchers.Main) {
            val br = brojPitanja
            /*val brKvitova = KvizRepository.getAl1l().size
            val svaMogucaPitanja : MutableList<Pitanje> = mutableListOf()
            for(i in KvizRepository.getAl1l()) {
                for(i in (KvizRepository.getAll()?.indices)!!) {
                    val pitanjaZaTajKviz = PitanjaKvizRepository.getPitanja(i)
                    svaMogucaPitanja.addAll(pitanjaZaTajKviz!!)
                }
            }*/

            //val nextFrag = FragmentPitanje(PitanjaKvizRepository.getPitanja(1)!!.get(br))
            val nextFrag = FragmentPitanje(svaPitanjaLista[br])
            nextFrag.arguments = bundle
            childFragmentManager.beginTransaction()
                .replace(R.id.framePitanje, nextFrag, "FragmentPitanje")
                .addToBackStack(null)
                .commit()
        }
    }





}
