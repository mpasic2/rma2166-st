package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.R

class FragmentPoruka : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) : View? {
        var view: View = inflater.inflate(R.layout.poruka_activity, container, false)
        var dajPoruku = this.arguments!!.getString("ovoJePoruka")
        val txt:TextView
        txt = view.findViewById(R.id.tvPoruka)
        txt.setText(dajPoruku)



        return view
    }

    companion object {
        fun newInstance(): FragmentPoruka = FragmentPoruka()
    }

}