package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.GrupaRepository
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository
import ba.etf.rma21.projekat.data.repositories.PredmetRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class FragmentPredmeti : Fragment(), AdapterView.OnItemSelectedListener {

    private lateinit var spinerGodina: Spinner
    private lateinit var  spinerPredmet: Spinner
    private lateinit var spinerGrupa: Spinner
    private lateinit var dodajDugme: Button

    private var brojac = 0


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) : View? {
        var view:View = inflater.inflate(R.layout.add_class_activity, container, false)

        spinerGodina = view.findViewById(R.id.odabirGodina)
        spinerPredmet = view.findViewById(R.id.odabirPredmet)
        spinerGrupa = view.findViewById(R.id.odabirGrupa)
        dodajDugme = view.findViewById(R.id.dodajPredmetDugme)


        ArrayAdapter.createFromResource(view.context, R.array.dropDownGodine,android.R.layout.simple_spinner_dropdown_item).also {
                adap -> adap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinerGodina.adapter = adap
        }
        spinerGodina.setSelection(zapamcenaGodina)


        spinerGodina.onItemSelectedListener=this
        spinerPredmet.onItemSelectedListener=this

        dodajDugme.setOnClickListener {
            GlobalScope.launch(Dispatchers.Main) {

            //System.out.println("OVO RADIIIIIIIIIIIIIIIIIIII!")
            //Toast.makeText(this@UpisiActivity, "HALOOOO", Toast.LENGTH_SHORT).show()
            //spasavamo odabrane podatke
            val godina = spinerGodina.selectedItem.toString()
            val predmet = spinerPredmet.selectedItem.toString()
            val grupa = spinerGrupa.selectedItem.toString()

            val porukica = Bundle()
            porukica.putString(
                "ovoJePoruka",
                "Uspješno ste upisani u grupu ${grupa} predmeta ${predmet}"
            )
            val frgmPoruka = FragmentPoruka.newInstance()
            frgmPoruka.arguments = porukica

            val taGrupa = GrupaRepository.getAllGroups()?.find { grp -> grp.naziv==grupa }



            PredmetIGrupaRepository.upisiUGrupu(taGrupa!!.id)

            val fm: FragmentManager? = fragmentManager
            requireActivity().supportFragmentManager?.beginTransaction()
                ?.replace(R.id.container, frgmPoruka, "fragment_screen")?.commit()


        }

        }

        spinerGrupa.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                zapamcenaGrupa = spinerGrupa.selectedItemPosition
                //TODO("Not yet implemented")
            }
            override fun onNothingSelected(parentView: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

        }
        )





        return view

    }



    companion object {
        fun newInstance(): FragmentPredmeti = FragmentPredmeti()
        var zapamcenaGodina : Int = 0
        var zapamceniPredmet : Int = 0
        var zapamcenaGrupa : Int = 0
    }


    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

        GlobalScope.launch(Dispatchers.Main) {
            //za sve
            if (parent?.id?.equals(R.id.odabirGodina)!!) {
                val listaPredmetaPoGodinama = PredmetIGrupaRepository.getPredmeti()?.filter { x -> x.godina==position+1 }
                val praznaLista: MutableList<String> = mutableListOf()

                val listaPredmeta: MutableList<String> = mutableListOf()
                for (predmet in PredmetRepository.getUpisani()) {
                    listaPredmeta.add(predmet.naziv)
                }

                for (predmet in listaPredmetaPoGodinama!!) {
                    praznaLista.add(predmet.naziv)
                }
                val listaPredmetaKojeTrebaPrikazat: MutableList<String> = mutableListOf()
                for (predm in praznaLista) {
                    if (listaPredmeta.contains(predm)) continue
                    else listaPredmetaKojeTrebaPrikazat.add(predm)
                }



                zapamcenaGodina = position
                spinerPredmet.adapter = ArrayAdapter<String>(
                    view!!.context,
                    android.R.layout.simple_spinner_dropdown_item,
                    listaPredmetaKojeTrebaPrikazat
                )
                spinerPredmet.setSelection(zapamceniPredmet)
                zapamcenaGodina = spinerGodina.selectedItemPosition

            }

            if (parent?.id?.equals(R.id.odabirPredmet)!!) {
                val listaGrupaPoPredmetima = GrupaRepository.getGroupsByPredmet(parent.selectedItem.toString()).toMutableList()
                val praznaLista: MutableList<String> = mutableListOf()
                for (grupe in listaGrupaPoPredmetima) {
                    praznaLista.add(grupe.naziv)
                }

                zapamceniPredmet = position
                spinerGrupa.adapter = ArrayAdapter<String>(
                    view!!.context,
                    android.R.layout.simple_spinner_dropdown_item,
                    praznaLista
                )

                spinerGrupa.setSelection(zapamcenaGrupa)
                zapamceniPredmet = spinerPredmet.selectedItemPosition


            }


        }

    }


}