package ba.etf.rma21.projekat.view

import android.content.Context
import android.graphics.Bitmap
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.repositories.KvizRepository.Companion.dajDatum
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*

class QuizListAdapter(
        private var kvizovi: List<Kviz>,
        private val onItemClicked: (kviz:Kviz) -> Unit
) : RecyclerView.Adapter<QuizListAdapter.quizViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): quizViewHolder {
        val view = LayoutInflater
                .from(parent.context)
                .inflate(R.layout.quiz_layout, parent, false)
        return quizViewHolder(view)
    }
    override fun getItemCount(): Int = kvizovi.size
    override fun onBindViewHolder(holder: quizViewHolder, position: Int) {
        //ovo uradit za kasnije
        val formatZaDatum: DateFormat = SimpleDateFormat("dd.MM.yyyy")
        //holder.nazivPredmeta.text = kvizovi[position].nazivPredmeta

        holder.nazivKviza.text = kvizovi[position].naziv
        //ovo se mora promijenit - ISPOD
        if(holder.nazivKviza.text=="Kviz 1") holder.nazivPredmeta.text ="IM1 RMA"
        if(holder.nazivKviza.text=="Kviz 2") holder.nazivPredmeta.text ="IM1"
        if(holder.nazivKviza.text=="Kviz 3") holder.nazivPredmeta.text ="RMA"


        holder.trajanje.text = kvizovi[position].trajanje.toString()
        holder.osvojeniBodovi.text = "100"
        /*if(kvizovi[position].osvojeniBodovi==null)
            holder.osvojeniBodovi.text = ""
        else
            holder.osvojeniBodovi.text = kvizovi[position].osvojeniBodovi.toString()*/

        //postavljanje slikice
        val datumKrajaKviza = kvizovi[position].datumKraj
        val prikaz: Context = holder.slikica.getContext()
        var id: Int = 0
        //za crvenu - nije raÄ‘en i datum proso
        /*if(kvizovi[position].osvojeniBodovi==null && datumKrajaKviza.before(Date())) {
            id = prikaz.getResources().getIdentifier("crvena", "drawable", prikaz.getPackageName())
            holder.datumKviza.text = formatZaDatum.format(kvizovi[position].datumKraj)
        }
        //za plavu - kviz proso, a uradje
        else if(datumKrajaKviza.before(Date()) && kvizovi[position].osvojeniBodovi!=null){
            id = prikaz.getResources().getIdentifier("plava", "drawable", prikaz.getPackageName())
            holder.datumKviza.text = formatZaDatum.format(kvizovi[position].datumRada)
        }*/
        //za zutu - tek treba bit aktivan
        if(dajDatum(kvizovi[position].datumPocetka).after(Date())){
            id = prikaz.getResources().getIdentifier("zuta", "drawable", prikaz.getPackageName())
            holder.datumKviza.text = formatZaDatum.format(kvizovi[position].datumPocetka)
        }
        //za zelena - aktivan, a nije uraÄ‘en
        else{
            id = prikaz.getResources().getIdentifier("zelena", "drawable", prikaz.getPackageName())
            if(kvizovi[position].datumKraj==null) holder.datumKviza.text = "nema kraja"
            else
                holder.datumKviza.text = formatZaDatum.format(kvizovi[position].datumKraj)
        }


        holder.slikica.setImageResource(id)
        holder.itemView.setOnClickListener{onItemClicked(kvizovi[position])}

    }
    fun azurirajKvizove(kvizovi: List<Kviz>) {

        this.kvizovi = kvizovi
        notifyDataSetChanged()
    }
    inner class quizViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nazivPredmeta: TextView = itemView.findViewById(R.id.nameid)
        val nazivKviza: TextView = itemView.findViewById(R.id.quizNameid)
        val datumKviza: TextView = itemView.findViewById(R.id.quizDateid)
        val trajanje: TextView = itemView.findViewById(R.id.QuizTimeid)
        val osvojeniBodovi: TextView = itemView.findViewById(R.id.QuizPointsid)
        val slikica: ImageView = itemView.findViewById(R.id.QuizImageid)


    }



}