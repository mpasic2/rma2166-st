package ba.etf.rma21.projekat.viewModel

import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.repositories.KvizRepository

class QuizListViewModel {

    suspend fun dajKvizoveSve():List<Kviz>{
        return KvizRepository.getAl1l();
    }

    suspend fun dajUrajdeneSve():List<Kviz>{
        return KvizRepository.getDone();
    }

    suspend fun dajBuduceKvizove():List<Kviz>{
        return KvizRepository.getFuture();
    }

    suspend fun dajNeUradjeneKvizove():List<Kviz>{
        return KvizRepository.getNotTaken();
    }

    suspend fun dajSveMojeKvizove():List<Kviz>{
        return KvizRepository.getMyKvizes();
    }


}
