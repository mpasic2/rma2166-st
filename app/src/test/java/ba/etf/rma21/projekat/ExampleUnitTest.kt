/*
package ba.etf.rma21.projekat

import ba.etf.rma21.projekat.data.*
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.GrupaRepository.Companion.getGroupsByPredmet
import ba.etf.rma21.projekat.data.repositories.KvizRepository
import ba.etf.rma21.projekat.data.repositories.PredmetRepository
import ba.etf.rma21.projekat.data.repositories.PredmetRepository.Companion.getPredmetsByGodina
import ba.etf.rma21.projekat.viewModel.QuizListViewModel
import org.junit.Test

import org.junit.Assert.*

*/
/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 *//*

class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun allFutureTests(){
        val sviBuduciKvizovi = QuizListViewModel().dajBuduceKvizove()
        assertEquals(sviBuduciKvizovi[0].osvojeniBodovi,null)
    }

    @Test
    fun allTestSorted(){
        val sviKvizovi = QuizListViewModel().dajKvizoveSve()
        assertEquals(sviKvizovi[0].datumPocetka.before(sviKvizovi[1].datumPocetka),true)
    }

    @Test
    fun allTakenTest(){
        val sviUradjeni = QuizListViewModel().dajUrajdeneSve()
        assertFalse(sviUradjeni[0].osvojeniBodovi==null)
    }

    @Test
    fun allNotTaken(){
        val dajKvizoveNeUradjene = QuizListViewModel().dajNeUradjeneKvizove()
        if(dajKvizoveNeUradjene.size!=0)
            assertEquals(dajKvizoveNeUradjene[0].osvojeniBodovi,null)
        else
            assertEquals(dajKvizoveNeUradjene.size,0)
    }

    @Test
    fun allTestsForSubjectandGroup(){
        assertTrue(dajSveKvizoveZaPredmetizaGrupu("RMA","Grupa 1").size>=0)
    }


    @Test
    fun allMyTests(){
        assertTrue(dajSveMojeKvizove().size>=0)
    }

   @Test
   fun checkAddingStudent(){
       val godina = "1"
       val predmet = "TP"
       val grupa = "Grupa 1"
       var ima = 0

        //simuliramo dodavanje
       KvizRepository.listaSvihMojihKvizova.addAll(dajSveKvizoveZaPredmetizaGrupu(predmet,grupa))
       val pred = Predmet(predmet,godina.toInt())
       listaSvihUpisanihPredmeta.add(pred)


       //provjeravamo da li je dodano
       for(i in listaSvihUpisanihPredmeta){
           if(i.naziv.equals("TP"))
               ima=1
       }

       assertEquals(ima,1)
   }

    @Test
    fun checkSubjectsByYear(){
        val godinaIspravna = 1
        val lista = getPredmetsByGodina(godinaIspravna)
        assertNotNull(lista)
        val godinaNeIspravna = 17
        val listaNull = getPredmetsByGodina(godinaNeIspravna)
        assertEquals(listaNull.size,0)

    }

    @Test
    fun checkGroupsBySubject(){
        val predmet = "RMA"
        val lista = getGroupsByPredmet(predmet)
        assertNotNull(lista)
    }





}*/
